﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TAC.Utils.TestModels;
using events.tac.local.Business.Navigation;

namespace Events.UnitTests
{
    [TestClass]
    public class NavigationModelBuilderTests
    {
        [TestMethod]
        public void ReturnsAModel()
        {
            TestItem item = new TestItem("test_1");
            var navigationModelBuilder = new NavigationModelBuilder();
            var model = navigationModelBuilder.CreateNavigationMenu(item, item);
            Assert.IsNotNull(model);

        }
    }
}



