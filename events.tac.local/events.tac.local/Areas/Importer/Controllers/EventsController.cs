﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using events.tac.local.Areas.Importer.Models;
using Newtonsoft.Json;
using Sitecore.Data;
using Sitecore.SecurityModel;
using Sitecore.Data.Items;

namespace events.tac.local.Areas.Importer.Controllers
{
    public class EventsController : Controller
    {
        // GET: Importer/Events
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(HttpPostedFileBase file, string parentPath)
        {
            IEnumerable<Event> events = null;
            string message = null;
            using (var reader = new System.IO.StreamReader(file.InputStream))
            {
                var contents = reader.ReadToEnd();
                try
                {
                    events = JsonConvert.DeserializeObject<IEnumerable<Event>>(contents); 
                }
                catch(Exception ex)
                {

                }
            }

            var database = Sitecore.Configuration.Factory.GetDatabase("master");
            var parentItem = database.GetItem(parentPath);
            var templateID = new TemplateID(new ID("{BCA7FE7C-9F02-4891-BA40-03293E30ED14}"));
            int countUpdateItems = 0;
            using (new SecurityDisabler())
            {
                foreach (var ev in events)
                {
                    var name = ItemUtil.ProposeValidItemName(ev.ContentHeading);

                    Item checkChildren = parentItem.Children[name];

                    Item item = checkChildren == null ? parentItem.Add(name, templateID) : checkChildren;
                    
                    item.Editing.BeginEdit();
                    item[Sitecore.FieldIDs.Workflow] = "{9BBCB780-19EC-4558-A587-5A44C24052EF}";
                    item[Sitecore.FieldIDs.WorkflowState] = "{DFFF89EF-BEAC-46DC-8C34-7086726B715E}";
                    item["ContentHeading"] = ev.ContentHeading;
                    item["ContentIntro"] = ev.ContentIntro;
                    item["Highlights"] = ev.Highlights;
                    item["StartDate"] = Sitecore.DateUtil.ToIsoDate(ev.StartDate);
                    item["Duration"] = Convert.ToString(ev.Duration);
                    item["Difficulty"] = Convert.ToString(ev.Difficulty);
                    countUpdateItems++;
                    item.Editing.EndEdit();
                }
            }

            ViewBag.CountUpdateItems = countUpdateItems;
            return View();
        }
    }
}