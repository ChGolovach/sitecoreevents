﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Sitecore.Data.Items;
using events.tac.local.Models;
using Sitecore.Links;
using Sitecore.Mvc.Presentation;
using events.tac.local.Business.Navigation;
using TAC.Utils.SitecoreModels;

namespace events.tac.local.Controllers
{
    public class NavigationController : Controller
    {

        private readonly NavigationModelBuilder _modelBuilder;
        private readonly RenderingContext _renderingContext;

        public NavigationController(NavigationModelBuilder modelBuilder, RenderingContext renderingContext)
        {
            _modelBuilder = modelBuilder;
            _renderingContext = renderingContext;
        }

        // GET: Navigation
        public ActionResult Index()
        {           
            //Item currentItem = RenderingContext.Current.ContextItem;
            Item currentItem = _renderingContext.ContextItem;
            Item section = currentItem.Axes.GetAncestors()
                .FirstOrDefault(i => i.TemplateName == "Events Section");
            //var model = CreateNavigationMenu(section, currentItem);
            var model = _modelBuilder.CreateNavigationMenu(new SitecoreItem(section) ,new SitecoreItem(currentItem));
            return View(model);
        }

        /*
        private NavigationMenu CreateNavigationMenu(Item root, Item current)
        {
            NavigationMenu menu = new NavigationMenu()
            {
                Title = root.DisplayName,
                URL = LinkManager.GetItemUrl(root),
                // Children = root.Axes.IsAncestorOf(current) ? root.GetChildren().Select(i => CreateNavigationMenu(i, current)) : null
                Active = root.ID == current.ID,
                Children = root.Axes.IsAncestorOf(current) ?
                                root.GetChildren()                                
                                .Where(i => i["ExcludeFromNavigation"] != "1")
                                .Select(i => CreateNavigationMenu(i, current)) : null
            };
            return menu;
        }*/
    }
    public static class BaseTemplate
    {
        public static bool IsBasedOn(this Item item,  Sitecore.Data.ID templateID)
        {
            var template = Sitecore.Data.Managers.TemplateManager.GetTemplate(item.TemplateID, item.Database);
            return template.DescendsFromOrEquals(templateID);
        }
    }

}