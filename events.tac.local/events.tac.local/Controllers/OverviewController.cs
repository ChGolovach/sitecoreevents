﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using events.tac.local.Models;
using Sitecore.Mvc.Presentation;
using Sitecore.Links;
using Sitecore.Web.UI.WebControls;


namespace events.tac.local.Controllers
{
    public class OverviewController : Controller
    {
        // GET: Overview
        public ActionResult Index()
        {
            var model = new OverviewList()
            {
                ReadMore = Sitecore.Globalization.Translate.Text("Read more")
            };
            model.AddRange(RenderingContext.Current.ContextItem.GetChildren(Sitecore.Collections.ChildListOptions.SkipSorting)
                .OrderBy(i => i.Statistics.Created)
                .Select(i => new OverviewItem()
                {
                    Url = LinkManager.GetItemUrl(i),
                    Title = new HtmlString(FieldRenderer.Render(i, "ContentHeading")),
                    Image = new HtmlString(FieldRenderer.Render(i, "DecorationBanner", "mw=500&mh=333"))
                }));
                //.OrderBy(i => RenderingContext.Current.Rendering.Properties["Created"]));
            return View(model);
        }
    }
}