﻿//using Sitecore.Analytics.Model.Entities;
using Sitecore.Analytics.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TAC.Utils.Mvc;
using Sitecore.Analytics.Outcome.Extensions;
using Sitecore.Data;
using Sitecore.Analytics.Outcome.Model;

namespace events.tac.local.Controllers
{
    public class SubscribeFormController : Controller
    {
        // GET: SubscribeForm
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ValidateFormHandler]
        public ActionResult Index(string email)
        {
            Sitecore.Analytics.Tracker.Current.Session.Identify(email);
            var contacts = Sitecore.Analytics.Tracker.Current.Contact;
            var emails = contacts.GetFacet<IContactEmailAddresses>("Emails");
            if (!emails.Entries.Contains("personal"))
            {
                emails.Preferred = "personal";
                var personalEmail = emails.Entries.Create("personal");
                personalEmail.SmtpAddress = email;
            }

            var outcome = new ContactOutcome(ID.NewID, new ID("{FE61E393-210D-4F36-B9C6-52A252481464}"), new ID(contacts.ContactId));
            Sitecore.Analytics.Tracker.Current.RegisterContactOutcome(outcome);

            return View("Confirmation");
        }
    }
}